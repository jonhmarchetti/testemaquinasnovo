SELECT h.maquina, 
	IF(l.entrada > h.entrada && l.latitude != 0 && l.longitude !=0 || h.latitude !=0 && l.longitude !=0, max(l.entrada), max(h.entrada)) as dataEntrada, 
	IF(l.entrada > h.saida && l.latitude != 0 && l.longitude !=0 || h.latitude !=0 && l.longitude !=0, NULL, max(h.saida)) as dataSaida, 
	IF(l.latitude != 0, l.latitude, h.latitude) as Latitude,
    IF(l.longitude != 0, l.longitude, h.longitude) as Longitude
FROM hist_geo h 

LEFT JOIN last_geo l ON l.maquina = h.maquina

GROUP BY l.maquina